﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjApp.Controllers
{
    public class UiRoutController : Controller
    {
        //
        // GET: /UiRout/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Home()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Home(string test)
        {
            return View();
        }
        public ActionResult Page1()
        {
            return View();
        }
        public ActionResult Page2()
        {
            return View();
        }
        public ActionResult Page3()
        {
            return View();
        }
        public ActionResult Page4()
        {
            return View();
        }
        public ActionResult Page1Left()
        {
            return PartialView();
        }
        public ActionResult Page1Right()
        {
            return PartialView();
        }
	}
}