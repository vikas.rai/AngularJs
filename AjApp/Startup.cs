﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AjApp.Startup))]
namespace AjApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
