﻿(function () {
    'use strict';
    var $routeProviderReference;
    angular.module('app')
        .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
            $locationProvider.hashPrefix('');
            $routeProviderReference = $routeProvider;
            //$routeProvider
            //    .when('/vikas', {
            //        templateUrl: '/Home/Page1',
            //        controller: 'AddStudentController'
            //    });

        }]);

    angular.module('app').run(['$route', '$http', '$rootScope',
    function ($route, $http, $rootScope, $location) {
        var a = $routeProviderReference;
        $('.mnu').each(function (i, item) {
        //    $(this).attr('href', '/#/' + window.atob($(this).attr('href')));
            var Fullpath = item.href.split("/");
            var path =Fullpath[Fullpath.length - 1];
            a.when('/' + path, {
                templateUrl: '/' + window.atob(path)
            });
        });
    }]);

})();

