﻿(function () {
    'use strict';
    angular.module('app')
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('UiRout', {
                    url: '/',
                    templateUrl: '/UiRout/Home'
                })
            .state('page1', {
                url: '/Page1',
                views: {
                    '@': {
                        templateUrl: '/UiRout/Page1'
                    },
                    'left@page1': {
                        templateUrl: '/UiRout/Page1Left'
                    },
                    'right@page1': {
                        templateUrl: '/UiRout/Page1Right'
                    }
                }
            })
                .state('page2', {
                    url: '/Page2',
                    templateUrl: '/UiRout/Page2'
                })
                .state('page3', {
                    url: '/Page3',
                    templateUrl: '/UiRout/Page3'
                });
        }]);
})();